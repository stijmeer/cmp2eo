Portfolio Stijn Meersschaert
============================
examenopdracht voor cmp2
------------------------
### Beschrijving
repository voor de examen opdracht voor het vak cross-media publishing, binnen de opleiding grafische en digitale media aan de arteveldehogeschool.

### Informatie
| Info |                    |
|:-----|:-------------------|
| OLOD | CMP 2              |
| Naam | Stijn Meersschaert |
| Groep| 2MMP proDEV        |

###Verklaring late milestones
Bij het uploaden van milestone 1 en 2 hebben er zich enkele problemen voorgedaan, waardoor u deze wellicht niet gevonden heeft.

| Milestone | Verklaring |
|:---------:|:-----------|
| 1         | Door problemen met de proxy-instellingen van Source Tree heb ik een hele tijd niks kunnen pushen naar git. Na contact op te nemen met mr Parent is dit probleem opgelost geraakt. Ik heb toen een gezipt bestand naar u gemaild als backup. Des gewenst kan ik u deze terug bezorgen. |
| 2         | Door een domme fout van mezelf heb ik deze deadline geupload naar Github ipv Bitbucket. Indien u de datum zou willen controleren kan u deze hier terugvinden: https://github.com/stijmeer/cmp2|