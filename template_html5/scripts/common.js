/**
 * Created by stijn on 6/03/2016.
 */
jQuery(document).ready(function ($) {
    var offset = $('nav').offset().top;
    $(window).scroll(function() {
        if($(this).scrollTop() >= offset) {
            $('.navigation').addClass('fixed');
        } else {
            $('.navigation').removeClass('fixed');
        }
    });

    $('.nav-trigger').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('nav-active');
    });
});