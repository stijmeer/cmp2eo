<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<?php $posts = get_posts( "numberposts=3" ); ?>
<!-- content -->
<main class="grid__container">
    <div class="grid__row grid__row_spacing_vrt_l">
        <div class="table">
            <div class="table-row">
                <?php foreach( $posts as $post ) : setup_postdata( $post ); ?>
                <section class="table-cell grid__column-bp1-10 grid__offset-bp1-1 grid__column-bp5-4 grid__offset-bp5-0 border-solid-yellow">
                    <i class="fa fa-newspaper-o fa-fw fa-4x yellow center_hor_text"></i>
                    <div class="decorative_lines_yellow center_hor_text">
                        <h2><?php echo $post->post_title; ?></h2>
                    </div>
                    <?php the_excerpt();?>
                    <a href="<?php echo get_permalink($post->ID); ?>">Read more &raquo;</a>
                    <time><?php the_time("l j, Y");?></time>
                </section>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
