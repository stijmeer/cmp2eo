/**
 * Created by stijn on 6/03/2016.
 */
jQuery(document).ready(function ($) {
    var offset = $('nav').offset().top;
    $(window).scroll(function() {
        if($(this).scrollTop() >= offset) {
            $('.navigation').addClass('fixed');
        } else {
            $('.navigation').removeClass('fixed');
        }
    });

    $('.nav-trigger').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('nav-active');
    });

    // Change menu alignment if user is logged in
    var listitems = $("#navigation nav ul li");
    var count = listitems.length;
    console.log(count);
    if ( count == 5) {
        listitems.each(function() {
            $(this).removeClass("grid__column-bp1-3");
            $(this).addClass("grid__column-bp1-2");
        });
        listitems.eq(0).addClass("grid__offset-bp1-1");
    }
});