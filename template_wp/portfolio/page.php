<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<main class="grid__container">
    <div class="grid__row grid__row_spacing_vrt_l">
        <div class="grid__column-bp1-3">
            <?php get_sidebar(); ?>
        </div>
        <div class="grid__column-bp1-9">

            <?php $temp = $wp_query; $wp_query= null;
            $wp_query = new WP_Query(); $wp_query->query('showposts=5' . '&paged='.$paged);
            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                <article class="grid__row grid__row_spacing_vrt_m border-solid-yellow">
                    <div class="grid__column-bp1-4">
                        <i class="fa fa-newspaper-o fa-fw fa-5x yellow center_hor_text"></i>
                        <div class="decorative_lines_yellow center_hor_text">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <time class="center_hor_text"> <i> <?php the_time("d/m/Y");?></i> </time>
                    </div>
                    <div class="grid__column-bp1-8">
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>" title="Read more">Read more &raquo;</a>
                        <?php if( has_tag() ) :?>
                            <p><?php the_tags(); ?></p>
                        <?php endif; ?>
                        <?php if ( ! has_category("uncategorized") ) :?>
                            <p>Categories:</p>
                            <?php the_category(); ?>
                        <?php endif; ?>
                    </div>
                </article>

            <?php endwhile; ?>

            <?php if ($paged > 1) { ?>

                <nav id="nav-posts" class="center_hor_text">
                    <div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
                    <div class="next"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
                </nav>

            <?php } else { ?>

                <nav id="nav-posts">
                    <div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
                </nav>

            <?php } ?>

            <?php wp_reset_postdata(); ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>

