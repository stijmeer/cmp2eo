<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <section class="error-404 not-found">
            <header class="page-header">
                <h3 class="page-title"><?php _e( 'The page you were looking for couldn&rsquo;t be found.' ); ?></h3>
            </header><!-- .page-header -->
            
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="button-yellow-white">Take me back to the home page</a>
        </section><!-- .error-404 -->

    </main><!-- .site-main -->

</div><!-- .content-area -->

<?php get_footer(); ?>
