<?php
/**
 * The template for displaying pages
 */

get_header(); ?>

<main>
    <div class="grid__container">
        <div class="grid__row grid__row_spacing_vrt_m">
            <div class="grid__column-bp1-12">
                <h2>Stijn Meersschaert Privacy Policy</h2>

                <p>Your privacy is important to us, and it is Stijn Meersschaert's policy to respect your privacy
                    regarding any information we may collect while operating our websites. Accordingly, we have
                    developed this Policy in order for you to understand how we collect, use, communicate and disclose
                    and make use of personal information. The following outlines our privacy policy.</p>

                <ul>
                    <li>Before or at the time of collecting personal information, we will identify the purposes for
                        which information is being collected.
                    </li>
                    <li>We will collect and use of personal information solely with the objective of fulfilling those
                        purposes specified by us and for other compatible purposes, unless we obtain the consent of the
                        individual concerned or as required by law.
                    </li>
                    <li>We will only retain personal information as long as necessary for the fulfillment of those
                        purposes.
                    </li>
                    <li>We will collect personal information by lawful and fair means and, where appropriate, with the
                        knowledge or consent of the individual concerned.
                    </li>
                    <li>Personal data should be relevant to the purposes for which it is to be used, and, to the extent
                        necessary for those purposes, should be accurate, complete, and up-to-date.
                    </li>
                    <li>We will protect personal information by reasonable security safeguards against loss or theft, as
                        well as unauthorized access, disclosure, copying, use or modification.
                    </li>
                    <li>We will make readily available to customers information about our policies and practices
                        relating to the management of personal information.
                    </li>
                </ul>

                <p>We are committed to conducting our business in accordance with these principles in order to ensure
                    that the confidentiality of personal information is protected and maintained. Stijn Meersschaert may
                    change its Privacy Policy from time to time, and at Stijn Meersschaert's sole discretion.</p>

            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>

