<?php
/**
 * The template for displaying the footer
 */
?>

<!-- footer -->
<footer class="bg-dGrey lGrey">
    <div class="grid__container">
        <div id="footer_socials" class="grid__row grid__row_spacing_vrt_m">
            <ul>
                <li class="grid__column-bp1-2">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class=" center_hor_text">
                        <div class="logo-background-lGrey">&nbsp;</div>
                        <p>home</p>
                    </a>
                </li>
                <li class="grid__column-bp1-2">
                    <a href="https://github.com/stijmeer" class="center_hor_text">
                        <i class="fa fa-github fa-fw"></i>
                        <p>github</p>
                    </a>
                </li>
                <li class="grid__column-bp1-2">
                    <a href="https://www.linkedin.com/in/stijn-meersschaert-646898113?trk=nav_responsive_tab_profile"
                       class="center_hor_text">
                        <i class="fa fa-linkedin fa-fw"></i>
                        <p>linkedin</p>
                    </a>
                </li>
                <li class="grid__column-bp1-2">
                    <a href="https://www.facebook.com/stijn.meersschaert" class="center_hor_text">
                        <i class="fa fa-facebook fa-fw"></i>
                        <p>facebook</p>
                    </a>
                </li>
                <li class="grid__column-bp1-2">
                    <a href="https://twitter.com/StijnMrsschrt" class="center_hor_text">
                        <i class="fa fa-twitter fa-fw"></i>
                        <p>twitter</p>
                    </a>
                </li>
                <li class="grid__column-bp1-2">
                    <a href="https://plus.google.com/114352912561710910911" class="center_hor_text">
                        <i class="fa fa-google-plus fa-fw"></i>
                        <p>google +</p>
                    </a>
                </li>
            </ul>
        </div>

        <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu',
                'container_class' => 'grid__row grid__row_spacing_vrt_m',
                'container_id' => 'footer_pages',
                'items_wrap' => '<ul>%3$s</ul>',
                'walker' => new footer_menu_walker()
            )
        ); ?>

        <div id="footer_rss">
            <div class="center_hor_text grid__row_spacing_vrt_s">
                <h3>Volg de rss feed:</h3>
                <a href="<?php bloginfo('rss2_url'); ?>"><?php bloginfo('rss2_url'); ?></a>
            </div>
            <div class="grid__row grid__row_spacing_vrt_s">
                <div class="table">
                    <?php
                    $link = esc_url( home_url( '/' ) ) . "feed";
                    $rss = new DOMDocument();
                    $rss->load( $link );
                    $feed = array();
                    foreach ($rss->getElementsByTagName('item') as $node) {
                        $item = array (
                            'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                            'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
                            'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
                            'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
                        );
                        array_push($feed, $item);
                    }
                    $limit = 5;
                    for($x=0;$x<$limit;$x++) {
                        $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
                        $link = $feed[$x]['link'];
                        $description = substr( $feed[$x]['desc'], 0, 150);
                        $date = date('l F j, Y', strtotime($feed[$x]['date']));

                        echo '<a href="'. $link . '" title="' . $title . '" class="table-row grid__column-bp1-12">';
                        echo    '<div class="table-cell grid__column-bp5-3 grid__column-bp1-12">' . $title . '</div>';
                        echo    '<div class="table-cell grid__column-bp1-12 grid__column-bp5-6 justify">' . $description . ' [...]</div>';
                        echo    '<time class="table-cell grid__column-bp1-1 grid__column-bp5-3 right" datetime="' . $date . '">' . $date . '</time>';
                        echo '</a>';
                    }

                    echo $proxylist_xml;
                    ?>

                </div>
            </div>

        </div>
    </div>
</footer>

<!-- js files -->
<script src="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/scripts/common.js" type="text/javascript"></script>

<?php wp_footer(); ?>
</body>
</html>
