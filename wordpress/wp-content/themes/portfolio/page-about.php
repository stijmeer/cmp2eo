<?php
/**
 * The template for displaying pages
 */

get_header(); ?>

<main class="bg-onethird-yellow">
    <div class="grid__container">
        <div class="grid__row grid__row_spacing_vrt_l">
            <div class="border-solid-yellow table full_width no-padding">
                <div class="grid__column-bp1-6" id="portfolio-image">
                    <h1>&nbsp; <br> &nbsp; <br> &nbsp;</h1>
                </div>
                <div class="grid__column-bp1-6">
                    <div class="padding-2">
                        <h2 class="yellow">Stijn <br> Meersschaert</h2>
                        <p>Student Grafische en Digitale Media aan de arteveldehogeschool, Gent.</p>
                        <p>Graphic Designer bij Horeca Belfine</p>
                        <p>Freelance Webdesigner en developer</p>
                        <br>
                        <a href="#" class="button-yellow-white center_hor_text">READ MY CV</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>

