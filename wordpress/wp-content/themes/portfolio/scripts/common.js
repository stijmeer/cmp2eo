/**
 * Created by stijn on 6/03/2016.
 */
jQuery(document).ready(function ($) {

    /*
     * Sticky header
     */

    var offset = $('nav').offset().top;
    $(window).scroll(function() {
        if($(this).scrollTop() >= offset) {
            $('.navigation').addClass('fixed');
        } else {
            $('.navigation').removeClass('fixed');
        }
    });

    $('.nav-trigger').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('nav-active');
    });

    /*
     * Change menu alignment if user is logged in
     */
    var listitems = $("#navigation nav ul li");
    var count = listitems.length;
    //console.log(count);
    if ( count == 5) {
        listitems.each(function() {
            $(this).removeClass("grid__column-bp1-3");
            $(this).addClass("grid__column-bp1-2");
        });
        listitems.eq(0).addClass("grid__offset-bp1-1");
    }

    /*
     * Contact form interaction
     */
    //$('.wpcf7-submit').addClass('button-yellow-dGrey');
    $('.wpcf7 input[type=text], input[type=email], input[type=tel], textarea').focus(function () {
        $(this).parent().parent().addClass('input-active');
    });
    $('.wpcf7 input[type=text], input[type=email], input[type=tel], textarea').focusout(function () {
        if ($(this).val() == "")  {
            $(this).parent().parent().removeClass('input-active');
        }
    });

    /*
     * Fix sidebar
     */
    var aside = $('#sidebar').parent();
    var main = $('#content');

    $(window).scroll(function () {
        var ws = $(this).scrollTop();
        var wm = main.height() + main.offset().top - aside.height();
        if (ws < wm - 80) {
            aside.css('position', 'relative');
            aside.css('z-index', 10);
            aside.css('top', $(this).scrollTop());
        }
    });
});