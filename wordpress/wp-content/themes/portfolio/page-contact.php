<?php
/**
 * The template for displaying pages
 */

get_header(); ?>

<main>
    <div class="grid__container">
        <div class="grid__row grid__row_spacing_vrt_m table full_width">
            <div class="table-row">
                <div class="grid__column-bp1-8 table-cell">
                    <?php echo do_shortcode('[wpgmza id="1"]');?>
<!--                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4999.39303665334!2d4.07132223072502!3d51.20624351834739!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c3889e4582aaef%3A0x1acd6f68c57da3c0!2sKemzeke%2C+9190+Stekene!5e0!3m2!1snl!2sbe!4v1461595967424" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                </div>
                <div class="grid__column-bp1-4 border-solid-yellow table-cell full_height">
                    <i class="fa fa-envelope-o fa-fw fa-4x yellow center_hor_text"></i>
                    <div class="decorative_lines_yellow center_hor_text">
                        <h2 class="no-margin">Gegevens</h2>
                    </div>
                    <h3 class="center_hor_text">Telefoon</h3>
                    <a href="tel:+32497033644" class="center_hor_text">+32 497 03 36 44</a>
                    <h3 class="center_hor_text">Email</h3>
                    <a href="mailto:stijnmeersschaert@gmail.com?Subject=Wordpress%20Redirect" target="_top" class="center_hor_text">stijnmeersschaert@gmail.com</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-dGrey yellow">
        <div class="grid__container">
            <div class="grid__row">
                <?php echo do_shortcode('[contact-form-7 id="34" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>

