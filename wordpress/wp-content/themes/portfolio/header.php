<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>
    <!-- Character Encoding -->
    <meta charset="<?php bloginfo('charset'); ?>">

    <!-- Latest version of the rendering engine -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Most important SEO friendly tag: title -->
    <title>Stijn Meersschaert | Portfolio</title>

    <!-- Other important SEO tags -->
    <meta name="description"
          content="Portfolio Stijn Meersschaert, in opdracht vor het vak 'Crossmedia Publishing' aan de Arteveldehogeschool.">
    <meta name="keywords"
          content="portfolio, work, projects, webdesign, webdevelopment, arteveldehogeschool, ahs, stijn, meersschaert, cmp, crossmedia productie, wordpress, template">
    <meta name="author" content="Stijn Meersschaert">
    <meta name="copyright" content="Copyright 2016 Stijn Meersschaert. All Rights Reserved.">

    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/manifest.json">
    <link rel="mask-icon" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/safari-pinned-tab.svg" color="#212121">
    <link rel="shortcut icon" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/css/images/favicons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Stijn Meersschaert | Portfolio">
    <meta name="application-name" content="Stijn Meersschaert | Portfolio">
    <meta name="msapplication-TileColor" content="#212121">
    <meta name="msapplication-TileImage" content="content/icons/mstile-144x144.png">
    <meta name="msapplication-config" content="content/icons/browserconfig.xml">
    <meta name="theme-color" content="#212121">

    <!-- Mobile Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,400italic|Vollkorn' rel='stylesheet'
          type='text/css'>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- MODERNIZR: New HTML5 elements and feature detection -->
    <script src="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/scripts/vendor/modernizr.js"></script>

    <!-- my stylesheet -->
    <link rel="stylesheet" type="text/css" href="http://localhost/cmp2/cmp_eo/wordpress/wp-content/themes/portfolio/style.css">

    <!-- wp head function -->
    <?php wp_head(); ?>
</head>

<body>

<?php if (is_front_page() || is_home()) : ?>
    <div class="full_width hero-image" style="max-height: 100vh; min-height: 100vh; position: relative;">
        <header class="full_height content_center_vrt">
            <div class="center_hor_text center_vrt">
                <div class="logo-background-yellow">
                    <h1>&nbsp;</h1>
                </div>
                <h1 class="yellow">Stijn Meersschaert</h1>
                <a href="portfolio" class="button-yellow-dGrey">Portfolio</a>
            </div>
        </header>
<?php endif; ?>

        <div id="navigation" <?php if (is_front_page() || is_home()) { ?> class="homepage" <?php } else { ?> class="page" <?php } ?> >

            <?php if ( is_user_logged_in() ) {
                $theme_location = 'primary-menu-logged-in';
            } else {
                $theme_location = 'primary-menu-logged-out';
            }
            wp_nav_menu(
                array(
                    'theme_location' => $theme_location,
                    'container' => 'nav',
                    'container_class' => 'grid__container navigation',
                    'items_wrap' => '<ul class="grid__row center_hor_text">%3$s</ul>',
                    'walker' => new primary_menu_walker()
                )
            );?>

        </div>

<?php if (is_front_page() || is_home()) : ?>
    </div>
<?php endif; ?>
