<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<main class="grid__container">
    <div class="grid__row grid__row_spacing_vrt_l">
        <article class="grid__column-bp1-8 border-solid-yellow">

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : ?>
                    <?php the_post(); ?>
                    <div>
                        <?php $image = get_field('afbeelding'); ?>
                        <?php if ($image): ?>
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" style="height: 5rem; width: auto; display: block; margin: 0 auto">
                        <?php else: ?>
                            <i class="fa fa-newspaper-o fa-fw fa-5x yellow center_hor_text"></i>
                        <?php endif; ?>
                        <div class="decorative_lines_yellow center_hor_text">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <p class="center_hor_text">Published on <?php the_time("l F j, Y");?> </p>
                    </div>
                    <div class="grid__offset-bp1-1 grid__column-bp1-10">
                        <?php the_content(); ?>
                        <b> <?php the_tags(); ?> </b>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>

        </article>

        <div class="grid__column-bp1-4">
            <?php /*echo do_shortcode('[bbp-forum-index]')*/ ?>
            <?php echo do_shortcode('[bbp-single-forum id=106]') ?>
        </div>

    </div>

    <?php comments_template(); ?>

</main>

<?php get_footer(); ?>
