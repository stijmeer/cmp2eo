<?php
/*
Plugin Name: My Portfolio
Plugin URI: http://gdm.gent/
Description: My Portfolio is the best plugin to store your own portfolio items.
Version: 1.0.0
Author: Kristof Raes
Author URI: http://krisra.gdm.gent/
*/

/*
- $post_type: (string) (vereist) Post type. (max. 20 karakters, geen hoofdletters of spaties toegelaten)
- $args: (array) (optioneel) Een array van argumenten. - In ons geval gaan we als post_type: 'portfolio' gebruiken. - In eerste instantie zijn de argumenten die we meegeven de volgende: labels, description, public, menu_position, supports, has_archive.
- Labels: zorgt ervoor dat je een string kan opgeven voor een bepaalde actie.
De labels dien je in een array toe te voegen. Zie voorbeeld hieronder.
Merk ook op dat er verschillende labels voorhanden zijn.
Voorbeeld voor de button om een item te bewerken zal je de label edit_item moeten gebruiken.
- Description: hierbij geef je een beschrijving voor de custom post type.
- Public: true; hierdoor zal volgende van toepassing zijn: exclude_from_search: false, publicly_queryable: true, show_in_nav_menus: true, and show_ui:true. De OOTB types attachment, page, and post hebben dezelfde instelling.
- Menu_position: 5; zal ervoor zorgen dat onze custom post type juist na posts zal weergegeven worden.
- Menu_icon: hierbij geven we mee welk icoontje we willen gebruiken bij de weergave in de linkse navigatie in de backoffice, voor onze sectie van ons custom post type.
- Supports: hierbij geven we een array met mogelijkheden wat betreft velden.
	- Default is dit  altijd title en editor.
		- Title: is de titel voor het item.
		- Editor: content van het item.
	- Thumbnail: afbeelding weergeven zoals mogelijk is via posts.
	- Excerpt:  is een optionele korte beschrijving van het item.
	- Comments: commentaar toevoegen.
	- Custom-fields: velden op maat.
- Has_archive: true; zal ervoor zorgen dat je een archief krijgt van uw post type.
Er zal default gebruik gemaakt worden van $post_type als slug voor het archief. Default is dit false.
*/

function custom_post_type_portfolio() {
    $labels = array(
        'name'               => _x( 'Portfolio', 'post type general name' ),
        'singular_name'      => _x( 'Portfolio item', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'book' ),
        'add_new_item'       => __( 'Add New Portfolio item' ),
        'edit_item'          => __( 'Edit Portfolio item' ),
        'new_item'           => __( 'New Portfolio item' ),
        'all_items'          => __( 'All Portfolio items' ),
        'view_item'          => __( 'View Portfolio item' ),
        'search_items'       => __( 'Search Portfolio items' ),
        'not_found'          => __( 'No portfolio item found' ),
        'not_found_in_trash' => __( 'No portfolio item found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Portfolio'
    );
  $args = array(
      'labels'        => $labels,
      'description'   => 'Holds our portfolio items specific data',
      'public'        => true,
      'menu_position' => 5,
      'menu_icon'     => 'dashicons-portfolio',
      'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'custom-fields'),
      'has_archive'   => true
  );
  register_post_type( 'portfolio', $args );
}
add_action( 'init', 'custom_post_type_portfolio' );

function taxonomies_portfolio() {
    $labels = array(
        'name'              => _x( 'Portfolio Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Portfolio Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Portfolio Categories' ),
        'all_items'         => __( 'All Portfolio Categories' ),
        'parent_item'       => __( 'Parent Portfolio Category' ),
        'parent_item_colon' => __( 'Parent Portfolio Category:' ),
        'edit_item'         => __( 'Edit Portfolio Category' ),
        'update_item'       => __( 'Update Portfolio Category' ),
        'add_new_item'      => __( 'Add New Portfolio Category' ),
        'new_item_name'     => __( 'New Portfolio Category' ),
        'menu_name'         => __( 'Portfolio Categories' )
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
    );
    register_taxonomy( 'portfolio_category', 'portfolio', $args );
}
add_action( 'init', 'taxonomies_portfolio', 0 );