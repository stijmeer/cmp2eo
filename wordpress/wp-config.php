<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2_portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!!V%[9u{=KJ$Ew`- Mc=P<goHD7VDqp Bk#+i13B_0tP=#lH1cP]Aufj0)j`,zG=');
define('SECURE_AUTH_KEY',  'tX5y,8B4{n9*qj/$#73??-o_m%hN;Wg2As2iwD}IYTuy.Y8e6)j#eAYs(t@u;Adj');
define('LOGGED_IN_KEY',    '41TBAe9w[-lp.h)ZikUaBL{=|fpGjqPS6<_ 6hX1X}Si#dnnqJ~(oA;cg**i&j*-');
define('NONCE_KEY',        'W}~6Z+A:tT;(vlxrY%9!o--iG&KR6My.243|KMopuZveTM9Hg.aFi&k;xXYNeV.2');
define('AUTH_SALT',        'XD]A5d`-K>jqS&)*+)iBmja)Jq_1?=,*:Eh&,B(Je+J62>G8>To!6W&iF>eD`SHM');
define('SECURE_AUTH_SALT', 'mS5j(F{?|OH$3GQDZ-RBwT77tN:L|+kMq!^9-.Zxv87>}S%F2p8NK~jBjj63>,Fu');
define('LOGGED_IN_SALT',   'Y3WJNVx!e2LM|{=e~L`R/D1E{1:QQAg(3M^Rx!up pvRNPbo%j|zEGO_8~,;E0[r');
define('NONCE_SALT',       '~_V]|[k$Y1VzuIl:lu4E2%K@oyV|L0MyUYBUhUSuuV`+_||_jRxq+ [/_O-Mjp&;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
